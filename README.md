# pubchem

A project for interactions with PubChem

* [PubChemLite Build Process Documentation](pubchemlite/README.org)

Content in this repository is shared under these license conditions:
* Code: Artistic-2.0
* Data: [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/legalcode)
