# Control the process of uploading to Zenodo

import json
import sys
import os
import os.path
import requests
import glob

fn_token=os.environ['FN_PBLCRITTER_TOKEN']
dir_zenodo=os.environ['DIR_PBLZENODO']
dir_work=os.environ['WORKDIR']

ZEN_DEPO_URL='https://sandbox.zenodo.org/api/deposit/depositions'
HEADERS={'Content-Type': 'application/json'}


DESCRIPTION="""WARNING: THIS DEPOSIT JUST FOR TESTING. THE CREATORS LIST IS ALSO
INCOMPLETE. DO NOT CITE, OR USE THIS SOURCE IN PRODUCTION USE, OR
PUBLICATIONS.

PubChemLite is a subset of PubChem
(https://pubchem.ncbi.nlm.nih.gov/) selected from major categories of
the Table of Contents page at the PubChem Classification Browser
(https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72). So far we
are providing two `flavours':

tier0 is compiled from 7 categories:
AgroChemInfo, DrugMedicInfo, FoodRelated, PharmacoInfo, SafetyInfo,
ToxicityInfo, KnownUse

tier1 is compiled from 8 categories (tier0 + BioPathway):
AgroChemInfo, BioPathway, DrugMedicInfo, FoodRelated, PharmacoInfo,
SafetyInfo, ToxicityInfo, KnownUse

PubChemCIDs have been collapsed by InChIKey first block, reporting the
structure from the most annotated CID, plus related CIDs. Entries that
will be ignored by MetFrag (salts, disconnected substances) or cause
errors (e.g. transition metals) have been removed. The Patent and
PubMed ID counts are extracted from files on the PubChem FTP site. The
`AnnoTypeCount' term counts how many of the categories are represented, the
subsequent column (named per category) counts the number of annotation
categories available in the next sub-category of the TOC entry.

These files can be used `as is' as localCSV for MetFrag Command Line
(https://ipb-halle.github.io/MetFrag/) - please do NOT upload these
files directly to the web interface, they are too large and will be
available in a drop-down menu.  """

TITLE="PubChemLite Tier 0 and Tier 1 Data"

CREATORS = [{'name': 'Bolton, Evan',
             'affiliation': 'NIH/NLM/NCBI',
             'orcid':'0000-0002-5959-6190'},
            {'name': 'Schymanski, Emma',
             'affiliation':'LCSB, University of Luxembourg',
             'orcid':'0000-0001-6868-8145'}]

with open(fn_token,'r') as f:
    token=f.read().rstrip()

ptoken={'access_token': token}

if not os.path.exists(dir_zenodo):
    os.makedirs(dir_zenodo)
    print("PBL directory created:" + dir_zenodo, file=sys.stderr)
    

# Create the repository, if it does not exist.
fn_repo=os.path.join(dir_zenodo,'repo.txt')
if os.path.exists(fn_repo):
    print("Retrieving Zenodo repository id from: " + fn_repo)
    with open(fn_repo,'r') as f:
        repo_id=f.read().rstrip()
else:
    r=requests.post(ZEN_DEPO_URL,params=ptoken,json={})
    if r.status_code == 201:
        repo_id='{}'.format(r.json()['id'])
        with open(fn_repo,'w') as f:
            f.write('{}'.format(r.json()['id']))
    else:
        print("(zenodo.py): Error. Problem accessing Zenodo. Tokens? Internet? Aborting.",file=sys.stderr)
        sys.exit(1)


    
# Get files

files = glob.glob(os.path.join(dir_work,'PubChemLite_tier*.csv'))
for fn in files:
    print(fn)

# Upload files

r = requests.get(ZEN_DEPO_URL+'/'+repo_id,params=ptoken,json={})
if not r.status_code==200:
    print('(zenodo.py): Unable to discover the repository ' + repo_id + '. Abort.',file=sys.stderr)
    sys.exit(1)

bucket_url = r.json()['links']['bucket']

mdata= {
    'metadata': {
        'title' : TITLE,
        'communities' : [{'identifier':'zenodo'}],
        'upload_type': 'dataset',
        'description': DESCRIPTION,
        'creators': CREATORS
    }
}

for fn in files:
    with open(fn,'rb') as f:
        dest="%s/%s" % (bucket_url,os.path.basename(fn))
        print('Uploading to ' + dest,file=sys.stderr)
        r = requests.put(dest,data=f,params=ptoken)
        if not r.status_code==200:
                print('(zenodo.py): Unable to upload file ' + fn ' to ' + repo_id + '. Abort.',file=sys.stderr)
                sys.exit(1)
        print('Done with ' + fn,file=sys.stderr)


r = requests.put(ZEN_DEPO_URL+'/'+repo_id,headers=HEADERS,data=json.dumps(mdata),params=ptoken)
if not r.status_code == 200:
    print('(zenodo.py): Unable to upload metadata to ' + repo_id + '. Abort.',file=sys.stderr)
    sys.exit(1)

r = requests.post(ZEN_DEPO_URL + '/' + repo_id + '/actions/publish',
                      params=ptoken)

if not r.status_code == 200:
    print('(zenodo.py): Unable to publish ' + repo_id + '. Abort.',file=sys.stderr)
    sys.exit(1)
