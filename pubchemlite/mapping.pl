use strict;
use warnings;

my $mapfile = $ARGV[0];

print STDERR ":: Using mappings file: \"$mapfile\" ::\n";

# The list of mappings.
my %map = ();
my @hdrl = ();
open(FMAP,$mapfile);
while ($_ = <FMAP>) {
    chop;
    my ($cty,$mfcol) = split(/\s*,\s*/,$_,2);
    push @hdrl,$mfcol;

}
my $hdr = join("",join("\t",@hdrl),"\n");


close(FMAP);
