PDIR=$(dirname "$BASH_SOURCE")
. "$PDIR/param.sh"


function say {
    echo -e "LOG LADY>" $@ >&2
    }

function header {
    say "[* START *]" $@
}

function footer {
    say "[* END *]" $@
}


function fetch {
    
    FN_SRC=$(readlink -f "$1")
    status=$?
    DEST_DIR=$2


    [ "${status}" -ne 0 ] && fatal "Error. The file containing the list of files to be
 downloaded does not exist.  "

    cd "${DEST_DIR}"
    say Downloading source files to "${DEST_DIR}"
    while read fn; do
	# Check for md5 hashes
	bfn=$(basename "$fn")
	mdfn="${bfn}.md5"
	rm -f "${mdfn}"
	say  "(fetch): Downloading ${fn}.md5"
	! (curl -s -f -R -O "${fn}.md5") &&
	    say  "(fetch): Warning: MD5 hash for file $fn not found."

	newhash=""
	if [ -e "$mdfn" ]; then
	    newhash=$(awk '{print $1}' "${mdfn}")
	fi
	
	if [ -e "$bfn" ] && [ -e "${mdfn}" ]; then
	    
	    oldhash=$(md5sum "${bfn}"|awk '{print $1}')
	    [[ "$newhash" == "$oldhash" ]] && \
		say  "(fetch): Skipping unchanged file $fn ." && \
		continue
	else
	    say  "(fetch): Downloading $fn"
	    ! (curl -s -f -R -O "$fn") && \
		fatal "(fetch): Error: File could not be downloaded from:" $fn ".Aborting"

	    dwnhash=$(md5sum "${bfn}"|awk '{print $1}')
	    [[ "$newhash" != "$dwnhash" ]] && \
		fatal "(fetch): Error: Download of $fn corrupted. Abort."
	fi
    done < "$FN_SRC"

    #exit 0
}



function fetch_wild {
    url=$1
    patt=$2
    dwn=$3
    wget -o wget.log -nd -P "$dwn" -e robots=off -w 1 --random-wait -l1 -r "$url" -A"$patt"
    say "Downloaded to $dwn"
}

function gen_tier_a {

    cidbits=$1
    filterfile=$2
    say CID bit file : "$cidbits" 
    say PubChemLite filter file : "$filterfile"
    say "Script: ${SCRIPTS[filter_toc_info]}"
    header BUILD filter_toc_info '(' $(date) ')'
    ( ${DCMPR} < "$cidbits" | "${SCRIPTS[filter_toc_info]}" "$filterfile" 1 | ${CMPR} > "./generated_bits.tsv.gz" )
    status=$?
    footer BUILD filter_toc_info '(' $(date) ')'
    [ "$status" -ne 0 ] && fatal "BUILD: filter_toc_info failed"
    return $status
}

function gen_tier_b {
    header BUILD pull_cid_content '(' $(date) ')'
    ( "${SCRIPTS[pull_cid_content]}" "./generated_bits.tsv.gz" > "./generated_data.out")
    status=$?
    footer BUILD pull_cid_content '(' $(date) ')'
    [ "$status" -ne 0 ] && fatal "BUILD: pull_cid_content failed"
    return $status
}

function gen_tier_c {
    header BUILD rest_grab_props '(' $(date) ')'
    legendfile=$1
    ( "${SCRIPTS[rest_grab_props]}" "$legendfile" < "./generated_data.out" | ${CMPR} > "./generated_data_complete.out.gz" )
    status=$?
    footer BUILD rest_grab_props '(' $(date) ')'
    [ "$status" -ne 0 ] && fatal "BUILD: rest_grab_props failed"
    return $status

}

function gen_tier_d {
    header BUILD remove_unwanted_cases '(' $(date) ')'
    ( ${DCMPR} < "generated_data_complete.out.gz" | "${SCRIPTS[remove_unwanted_cases]}" "generated_" > "$OUTMFFILE" )
    status=$?
    footer BUILD remove_unwanted_cases '(' $(date) ')'
    [ "$status" -ne 0 ] && fatal "BUILD: rest_unwanted_cases failed"
    return $status
}



function gen_tier {
    set -o pipefail		# this will capture a problem that
				# happens while piping
    cidbits=$1
    filterfile=$2
    legendfile=$3
    gen_tier_a "$cidbits" "$filterfile" && \
	gen_tier_b && \
	gen_tier_c "$legendfile" && \
	gen_tier_d
}

function gen_tier_test {
    set -o pipefail		# this will capture a problem that
				# happens while piping
    cidbits=$1
    filterfile=$2
    legendfile=$3
    # gen_tier_c "$legendfile" && \
    # gen_tier_d

}

function stamp {
    header STAMP
    say "Current directory is $PWD"
    say "Input files are located in $INPUTDIR"
    say "The top-level directory used by PCL is $TOPDIR"
    say "The build directory is $WORKDIR"
    say "The filter file used is $FILTERFILE"
    say "The MetFrag files are going to be written to the following dirs (MF_DIRS): ${MF_DIRS[@]}"
    say "The output MetFrag file will be $OUTMFFILE in the MF_DIRS"
    say "The output MetFrag file will also be saved as $OUTMFFILE the MF_DIRS"
    say "The scripts have been located in $SCRIPTDIR"
    say "The full build result is going to be stored in $OUTDIR"
    say "Legend file is $LEGENDFILE"
    footer STAMP
}

function gen_filtfile  {
    map=$1
    loc=$2
    fname="$loc/$3"

    say "(gen_filtfile) Generating filter file: $fname"
    awk 'BEGIN {FS=","}
         NR==1 {next}
         NR>1 {print($1,$2)}' \
	     "$map" > "$fname"
    
}

function gen_legend {
    map=$1
    loc=$2
    fname="$loc/$3"

    say "(gen_legend) Generating legend file: $fname"
    awk 'BEGIN {FS=",";OFS=","}
         NR==1 {next}
         NR>1 {print($2,$3)}' \
	     "$map" > "$fname"
}
