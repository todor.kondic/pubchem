## This is the script "constants" file. Change when architecture
## changes.



NTHREAD=6    # Number of threads for (de)compression.
CMPR="pigz -p ${NTHREAD}"    # Thing to use for compression.
DCMPR="pigz -p ${NTHREAD} --decompress"    # Thing to use for decompression.

## Destination directory.
DDIR=$PWD

# Program auxiliary files.
PDIR=$(dirname "$BASH_SOURCE")

# Destination directory for perl and other scripts.
SCDIR=$DDIR

declare -A SCRIPTS
SCRIPTS=([filter_toc_info]="$SCDIR/filter_toc_info.pl" \
	 [pull_cid_content]="$SCDIR/pull_cid_content.pl" \
	 [rest_grab_props]="$SCDIR/rest_grab_props.pl" \
	 [remove_unwanted_cases]="$SCDIR/remove_unwanted_cases.pl")

export PCL_DIRSRC=$SCRIPTDIR/..
export PCL_DIRWRK=$WORKDIR
export PCL_RUN_NAME=exposomics
export PCL_DIR_TRACK=${MF_DIRS[0]}/eval4pub
# A bit dangerous, because this should be a single file.
export PCL_FN_METFRAG_DB=$(echo "${WORKDIR}"/PubChemLite_exposomics_*.csv)

# This will run in eval4pub.sh
export PCL_EVALSCRIPT=${SCRIPTDIR}/R/eval4pub/eval4pub.R
