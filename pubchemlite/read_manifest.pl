# Read in YAML and generate a list of entries that can be used by a
# bash script to declare variables. Each entry is preceded by either
# 's' for scalar, or 'a' for an array value.

use strict;
use warnings;
use YAML::XS 'LoadFile';
use Data::Dumper;
    
my $config = LoadFile($ARGV[0]);
print("s MAPFILE"," ",$config->{map},"\n");
print("s TOPDIR ",$config->{topdir},"\n");
print("s OUTDIR ",$config->{outdir},"\n");
my $mfdirs = join(" ",map { join("","\'",$_,"\'") } @{$config->{mf_dirs}});
print("a MF_DIRS ","( ",$mfdirs," )","\n");

