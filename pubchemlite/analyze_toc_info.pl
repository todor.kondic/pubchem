#!/usr/bin/perl

#3       KEGG: Endosymbionts     Ontologies      Classification
#4       Associated Article      Crystal Structures      Structures

use strict;

my %u = ();
my %v = ();
my %w = ();
my %x = ();

my @b = ();
open( TAB, "index.tsv" ) || die "Unable to read index.tsv!\n";
while ( $_ = <TAB> ) {
  chop;
  my @tmp = split( /	/, $_ ) ;
  my $ntmp = @tmp;

  if ( $ntmp == 2 ) {
    my $f = pop( @tmp );
    $u{ $f } = $tmp[0];
  } elsif ( $ntmp == 3 ) {
    my $f = pop( @tmp );
    my $s = pop( @tmp );
    $v{ $f }{ $s } = $tmp[0];
  } elsif ( $ntmp == 4 ) {
    my $f = pop( @tmp );
    my $s = pop( @tmp );
    my $t = pop( @tmp );
    $w{ $f }{ $s }{ $t } = $tmp[0];
  } elsif ( $ntmp == 5 ) {
    my $f = pop( @tmp );
    my $s = pop( @tmp );
    my $t = pop( @tmp );
    my $r = pop( @tmp );
    $x{ $f }{ $s }{ $t }{ $r } = $tmp[0];
  } else {
    print STDERR "SKIPPED :: $_\n";
    next;
  }

}
close( TAB );

foreach my $u ( sort( keys( %u ) ) ) {
  printf( "%-4d", $u{$u} );
  print "  $u\n";
  foreach my $v ( sort( keys( %{ $v{ $u } } ) ) ) {
    printf( "%-4d", $v{$u}{$v} );
    print "      $v\n";
    foreach my $w ( sort( keys( %{ $w{ $u }{ $v } } ) ) ) {
      printf( "%-4d", $w{$u}{$v}{$w} );
      print "         $w\n";
      foreach my $x ( sort( keys( %{ $x{ $u }{ $v }{ $w } } ) ) ) {
        printf( "%-4d", $x{$u}{$v}{$w}{$x} );
        print "         $x\n";
      }
    }
  }
}

