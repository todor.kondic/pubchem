#!/usr/bin/perl

use strict;
use warnings;


my $file = $ARGV[0];

print STDERR ":: Using file: \"$file\" ::\n";

my $use_counts = $ARGV[1];
if ( ! defined( $use_counts ) || $use_counts != 1 ) {
  $use_counts = 0;
}
if ( $use_counts == 1 ) {
  print STDERR ":: Using counts (tab-delim set of composite bits) ::\n";
} else {
  print STDERR ":: Not using counts (binary string) ::\n";
}


# Read in binary bit locations and names
my %bn = ();  # Bit - Name correspondance
my %nb = ();  # Name - Bit correspondance
open( TAB, "index.tsv" ) || die "Unable to read index.tsv!\n";
while ( $_ = <TAB> ) {
  chop;
  my ( $bit, $name, $tmp ) = split( /	/, $_, 3 );
  $nb{ $name } = $bit;
  $bn{ $bit } = $name;
}
close( TAB );


# Read file containing the set of bits to include
my $ifp = 0;
my @l = ();
my %l = ();
my %f = ();
my @f = ();
open( TAB, "$file" ) || die "Unable to read $file!\n";
while ( $_ = <TAB> ) {
  chop;
  my ( $f, $l ) = split( /\s+/, $_, 2 );

  if ( defined( $nb{ $l } ) &&
       $nb{ $l } != $f ) {
    print STDERR "WARNING :: \"$l\" corresponds to bit \"$nb{$l}\" (not \"$f\")\n";
    $f = $nb{ $l };
  } elsif ( defined( $bn{ $f } ) &&
            $bn{ $f } ne "$l" ) {
    print STDERR "WARNING :: bit \"$f\" corresponds to name \"$bn{$f}\" (not \"$l\")\n";
    $l = $bn{ $f };
  }
  push( @f, $f );
  push( @l, $l );

  $l{ $l } = $f;
  $f{ $f } = $ifp;
  $ifp++;
}
close( TAB );
print "\t", join( " ", @f ), "\n";
print STDERR "\t", join( " ", @f ), "\n";
print "\t", join( "\t", @l ), "\n";
print STDERR "\t", join( "\t", @l ), "\n";


# Open the file containing the TOC data structure
my %ff = ();  # Fingerprint location and next level FP locations and FP where to impute
my @ff = ();
if ( $use_counts == 1 ) {
  my %s = ();  # Fingerprint labels we need to sum for a given fingerprint value
  my %sl = ();  # Fingerprint labels to FP location tracking

  open( TAB, "index.tsv" ) || die "Unable to read index.tsv!\n";
  while ( $_ = <TAB> ) {
    chop;
    my @tmp = split( /	/, $_ );
    my $f = shift( @tmp );  # Fingerprint location
    my $ntmp = @tmp;

    if ( $ntmp > 1 ) {
      if ( defined( $l{ $tmp[1] } ) ) {
        $ff{ $l{ $tmp[1] } }{ $f } = $f{ $l{ $tmp[1] } };  # Group the lower level FPs to this one
        $s{ $tmp[0] } = $tmp[1];  # Track label for later report
        $sl{ $tmp[0] } = $f;  # Track FP location for later report
      }
    }
  }
  close( TAB );

  print STDERR "\t\n:: Summing imputed fingerprints as such ::\n\tFP Label\tCounts towards FP Sum\n";
  foreach my $s ( sort( keys( %s ) ) ) {
    print STDERR "\t$sl{$s} $s\t$l{$s{$s}} $s{$s}\n";
  }

}

#5       1000000000000000000000000000000011000001000000000000000000000000000000000001100100000000000000000000000000000000000000000000000000000000000000000000000000000000000000100000000000001000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110010010000000100000000000000000000000000000000000000000000000000000000000000000000010000100000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110100000000000000000000000001001000000000000000000000000000

my @c = ();
my $nc = 0;
foreach my $f ( @f ) {
  $c[ $nc++ ] = 0;
}

if ( $use_counts == 1 ) {
  while ( $_ = <STDIN> ) {
    chop;
    my ( $cid, $fp ) = split( /	/, $_, 2 );

    my $do_print = 0;
    my @tc = ();
    my $tc = 0;
    foreach my $f ( @f ) {
      my $b = substr( $fp, ( $f - 1 ), 1 );
      if ( $b eq "1" ) {
        $c[ $tc ]++;
        $do_print = 1;
      }

      if ( defined( $ff{ $f } ) ) {
        my @ff = sort bynum( keys( %{ $ff{ $f } } ) );
        foreach my $ff ( sort bynum( keys( %{ $ff{ $f } } ) ) ) {
          $b .= substr( $fp, ( $ff - 1 ), 1 );
        }
      }

      $tc[ $tc++ ] = $b;
    }

    if ( $do_print != 0 ) {
      print "$cid\t", join( "	", @tc ), "\n";
    }
  }
} else {
  while ( $_ = <STDIN> ) {
    chop;
    my ( $cid, $fp ) = split( /	/, $_, 2 );

    my $tc = 0;
    my $v = "";
    foreach my $b ( @f ) {
      my $p = substr( $fp, ( $b - 1 ), 1 );
      $v .= "\t" . $p;
#      print "$b  ", substr( $fp, ( $b - 1 ), 1 ), "  ";
      if ( $p eq "1" ) {
        $c[ $tc ]++;
      }
      $tc++;
    }

    if ( $v =~ /1/ ) {
      print "$cid", "$v\n";
#    } else {
#      print STDERR "$cid\t$v\n";
    }
  }
}

print STDERR "\n:: Summary Counts for desired Fingerprints ::\nCount\tFP Location\tFP Label\n";
for ( my $i = 0; $i < $nc;  $i++ ) {
  print STDERR "$c[$i]\t$f[$i]\t$l[$i]\n";
}


sub bynum { $a <=> $b };

