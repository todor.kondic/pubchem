# PubChemLite Evaluation - Prepare MetFrag
# A script to prepare all files necessary for MetFrag
# Includes download of MetFrag CL and all local CSV files
# Note many of these downloads are >200 MB!
# Emma Schymanski, 28 Oct 2020. 
# Partner script to PubChemLiteEval_merged.R


# For this script to run, several files need to be downloaded.
# It may be easiest to download manually (all MetFrag CSV are >~200 MB)

###### Set directories (please adjust to system)######
# Note: keep these consistent with PubChemLiteEval scripts

# by default this will create subfolders of the current directory
base_dir <- getwd()

# The MetFrag Local CSVs will be saved here:
db_dir <- paste0(base_dir, "/MetFrag_CSVs/")

# The MetFrag jar will be saved here:
metfrag_dir <- paste0(base_dir, "/MetFrag/")


#### Download MetFrag CL #####

# First step, get latest MetFrag CL version
file_url <- "http://msbi.ipb-halle.de/~cruttkie/metfrag/MetFrag2.4.5-CL.jar"
download.file(file_url, paste0(metfrag_dir,"MetFrag2.4.5-CL.jar"))


##### Download Local CSV files ######

# Next, download all the local CSV files

# PubChemLite Nov 2019 tier0 and tier1
file_url <- "https://zenodo.org/record/3548654/files/PubChemLite_18Nov2019_tier0.csv"
download.file(file_url, paste0(db_dir,"PubChemLite_18Nov2019_tier0.csv"))

file_url <- "https://zenodo.org/record/3548654/files/PubChemLite_18Nov2019_tier1.csv"
download.file(file_url, paste0(db_dir,"PubChemLite_18Nov2019_tier1.csv"))


# PubChemLite Jan 2020 tier0 and tier1
file_url <- "https://zenodo.org/record/3611238/files/PubChemLite_14Jan2020_tier0.csv"
download.file(file_url, paste0(db_dir,"PubChemLite_14Jan2020_tier0.csv"))

file_url <- "https://zenodo.org/record/3611238/files/PubChemLite_14Jan2020_tier1.csv"
download.file(file_url, paste0(db_dir,"PubChemLite_14Jan2020_tier1.csv"))


# CompTox Select Metadata file:
file_url <- "ftp://newftp.epa.gov/COMPTOX/Sustainable_Chemistry_Data/Chemistry_Dashboard/MetFrag_metadata_files/CompTox_17March2019_SelectMetaData.csv"
download.file(file_url, paste0(db_dir, "CompTox_17March2019_SelectMetaData.csv"))

# get interim PubChemLite versions used for paper evaluation
zenodo_url <- "https://zenodo.org/record/4146957/files/"
file_url <- paste0(zenodo_url, "PubChemLite_tier0_20200522.csv")
download.file(file_url, paste0(db_dir,"PubChemLite_tier0_20200522.csv"))

zenodo_url <- "https://zenodo.org/record/4146957/files/"
file_url <- paste0(zenodo_url, "PubChemLite_tier0_20200612_mod.csv")
download.file(file_url, paste0(db_dir,"PubChemLite_tier0_20200612_mod.csv"))


##### Download all necessary MS/MS and mapping files ######

zenodo_url <- "https://zenodo.org/record/4146957/files/"

# get the MS/MS for CASMI / MetFragRL evaluations
file_url <- paste0(zenodo_url,"MSMS.zip")
download.file(file_url,paste0(base_dir,"MSMS.zip"))
unzip(paste0(base_dir,"MSMS.zip"))

# get the temporary / dummy MS/MS files needed for MetFrag
file_url <- paste0(zenodo_url,"dummy_msms.txt")
download.file(file_url, paste0(metfrag_dir,"dummy_msms.txt"))
# if there is no temp MSMS file, create this (it's overwritten in scripts)
if(!file.exists(paste0(metfrag_dir,"MetFrag_temp_msms.txt"))) {
  file.copy(paste0(metfrag_dir,"dummy_msms.txt"), 
            paste0(metfrag_dir,"MetFrag_temp_msms.txt"))
}

# get the mapping file needed for CASMI:
file_url <- paste0(zenodo_url,"solutions_casmi2016_cat2and3.csv")
download.file(file_url, paste0(base_dir,"solutions_casmi2016_cat2and3.csv"))

# get the PCLite Benchmarking file needed (also in main script)
#zenodo_url <- "https://zenodo.org/record/4146957/files/"
file_url <- paste0(zenodo_url, "PCLite_Benchmark_wMSMS_info.csv")
download.file(file_url, paste0(base_dir,"PCLite_Benchmark_wMSMS_info.csv"))

# get the AgroChem file: 
file_url <- paste0(zenodo_url, "AgroChemicals_fromPCLite_14Jan2020_tier1.csv")
download.file(file_url, paste0(base_dir,
                               "AgroChemicals_fromPCLite_14Jan2020_tier1.csv"))


# get the agrochem InChIKey First Block (IKFB) list
# plus the other subsets investigated (tab files)

file_url <- paste0(zenodo_url,"benchmark_agchem_ikfb.lst")
download.file(file_url, paste0(base_dir,"benchmark_agchem_ikfb.lst"))

file_url <- paste0(zenodo_url,"benchmark_DrugMedicInfo_ikfb.tab")
download.file(file_url, paste0(base_dir,"benchmark_DrugMedicInfo_ikfb.tab"))

file_url <- paste0(zenodo_url,"benchmark_KnownUse_ikfb.tab")
download.file(file_url, paste0(base_dir,"benchmark_KnownUse_ikfb.tab"))

file_url <- paste0(zenodo_url,"benchmark_ToxicityInfo_ikfb.tab")
download.file(file_url, paste0(base_dir,"benchmark_ToxicityInfo_ikfb.tab"))


