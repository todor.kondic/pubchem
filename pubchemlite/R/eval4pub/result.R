## Class `result' is there to simply present the results in a
## consistent manner.


new_result <- setClass("result", representation = list(n_entries = "integer",
                                                       IKFB_r1 = "integer",
                                                       IKFB_fail = "integer",
                                                       IKFB_rle2 = "integer",
                                                       IKFB_rle5 = "integer",
                                                       IKFB_numeric = "integer"))


result <- function(cmpd_info) {
    n_entries <- NROW(cmpd_info)
    IKFB_r1 <- length(which(cmpd_info$IKeyFBlock_rank == 1))
    IKFB_fail <- length(which(is.na(cmpd_info$IKeyFBlock_rank)))
    IKFB_rle2 <- length(which(cmpd_info$IKeyFBlock_rank <= 2))
    IKFB_rle5 <- length(which(cmpd_info$IKeyFBlock_rank <= 5))
    IKFB_numeric <- length(which(cmpd_info$IKeyFBlock_rank > 0))
    new_result(n_entries = n_entries,
               IKFB_r1 = IKFB_r1,
               IKFB_fail = IKFB_fail,
               IKFB_rle2 = IKFB_rle2,
               IKFB_rle5 = IKFB_rle5,
               IKFB_numeric = IKFB_numeric)
}

setMethod("logme",signature = "result" ,function (self,pref = "(PCL Eval)") {
    IKFB_r1 <- self@IKFB_r1
    IKFB_rle2 <- self@IKFB_rle2
    IKFB_rle5 <- self@IKFB_rle5
    IKFB_fail <- self@IKFB_fail
    n_entries <- self@n_entries
    lines <- lapply(slotNames(self),function(nm) paste0(nm,": ",slot(self,nm)))
    lines <- c(lines,
               paste0("IKFB_r1/n_entries: ", IKFB_r1/n_entries),
               paste0("IKFB_rle2/n_entries: ", IKFB_rle2/n_entries),
               paste0("IKFB_rle5/n_entries: ", IKFB_rle5/n_entries),
               paste0("IKFB_fail/n_entries: ", IKFB_fail/n_entries))
    do.call(logblock,c(list(pref=pref,
                            blockname=as.character(class(self))),
                       lines))
    
})

setMethod("write",signature = "result", function(x=x,file=file,time = Sys.time(),...) {
    df <- if (file.exists(file)) read.csv(file,stringsAsFactors = F,colClasses = c(time="POSIXct")) else data.frame(time = Sys.time()[0],
                                                                                     r1 = integer(0),
                                                                                     rle2 = integer(0),
                                                                                     rle5 = integer(0),
                                                                                     fail = integer(0),
                                                                                     n_entries = integer(0),
                                                                                     rel_r1 = numeric(0),
                                                                                     rel_rle2 = numeric(0),
                                                                                     rel_rle5 = numeric(0),
                                                                                     rel_fail = numeric(0))
    
    IKFB_r1 <- x@IKFB_r1
    IKFB_rle2 <- x@IKFB_rle2
    IKFB_rle5 <- x@IKFB_rle5
    IKFB_fail <- x@IKFB_fail
    n_entries <- x@n_entries
    lst <- list(time = time,
                r1 = IKFB_r1,
                rle2 = IKFB_rle2,
                rle5 = IKFB_rle5,
                fail = IKFB_fail,
                n_entries = n_entries)

    lst$rel_r1 <- lst$r1/lst$n_entries
    lst$rel_rle2 <- lst$rle2/lst$n_entries
    lst$rel_rle5 <- lst$rle5/lst$n_entries
    lst$rel_fail <- lst$fail/lst$n_entries
    tmp <- data.frame(lst)
    res <- rbind(df,tmp)
    write.csv(x=res,file=file, row.names=F)
    res
})
