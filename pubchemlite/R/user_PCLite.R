# Customizable MetFrag reporting functions
# Emma Schymanski, Todor Kondic
# 11 May 2020
# PubChemLite basic options
# extending to extra terms as well ... 

# AgroChemInfo, BioPathway, DrugMedicInfo, FoodRelated, 
# PharmacoInfo, SafetyInfo, ToxicityInfo, KnownUse
# NOTE: BioPathway is tier1 only

user_mk_cmpd_info <- function(cmpd_info,IKmatch=TRUE,extra_terms = c("none")) {
    # set up the extra terms
    test_terms <- grep(TRUE, extra_terms %in% c("agro","bio","drug","food", "pharma", 
                                                "safety", "toxicity", "use", "none"))
    if(length(test_terms)<1) {
        stop("Incorrect extra terms: select one or more of agro, bio, drug, 
             food, pharma, safety, toxicity, use, none")
    }
    
    ## Set up new columns
    cmpd_info$msms_avail <- FALSE
    cmpd_info$msms_peaks <- ""
    ## Basic MetFrag summary: max score of 4 or 5 with FPSum/AnnoTypeCount
    cmpd_info$num_poss_IDs <- ""
    cmpd_info$max_Score <- ""
    cmpd_info$n_Score_GE4 <- ""
    #cmpd_info$n_Score_GE3 <- ""
    cmpd_info$n_Score_GE2 <- ""
    # basic metadata summary
    cmpd_info$n_MoNA_GEp9 <- ""
    cmpd_info$AutoLevel <- ""
    
    ## Summary of top candidate and max metadata values 
    cmpd_info$CID_maxScore <- ""
    cmpd_info$SMILES_maxScore <- ""
    cmpd_info$InChIKey_maxScore <- ""
    cmpd_info$Name_maxScore <- ""
    cmpd_info$ExplPeaks_maxScore <- ""
    # top candidate
    cmpd_info$SMILES_TopCand <- ""
    cmpd_info$Name_TopCand <- ""
    cmpd_info$CID_TopCand <- ""
    cmpd_info$ExplPeaks_TopCand <- ""
    cmpd_info$MoNA_TopCand <- ""
    if (IKmatch) {
        cmpd_info$RankSuspect <- ""
        cmpd_info$TopCandIsSuspect <- ""
    }
    
    ## Max of individual categories
    ## MetFrag
    cmpd_info$max_NoExplPeaks <- ""
    cmpd_info$NumberPeaksUsed <- ""
    cmpd_info$max_FragmenterScore <- ""
    ## MoNA
    #cmpd_info$max_MetFusion <- ""
    cmpd_info$max_MoNAIndiv <- ""
    #PubChem Scores: PubMed_Count,Patent_Count,Annotation_Count
    # note for earliest PubChemLite, AnnoCount is FPSum, after AnnoTypeCount
    cmpd_info$max_PubMedCount <- ""
    cmpd_info$max_PatentCount <- ""
    cmpd_info$max_AnnoCount <- ""
    # Extra annotation scores: 
    if (length(grep("agro",extra_terms))>0) {
        cmpd_info$max_AgroChemInfo <- ""
    }
    if (length(grep("bio",extra_terms))>0) {
        cmpd_info$max_BioPathway <- ""
    }
    if (length(grep("drug",extra_terms))>0) {
        cmpd_info$max_DrugMedicInfo <- ""
    }
    if (length(grep("food",extra_terms))>0) {
        cmpd_info$max_FoodRelated <- ""
    }
    if (length(grep("pharma",extra_terms))>0) {
        cmpd_info$max_PharmacoInfo <- ""
    }
    if (length(grep("safety",extra_terms))>0) {
        cmpd_info$max_SafetyInfo <- ""
    }
    if (length(grep("toxicity",extra_terms))>0) {
        cmpd_info$max_ToxicityInfo <- ""
    }
    if (length(grep("use",extra_terms))>0) {
        cmpd_info$max_KnownUse <- ""
    }
    
    ## Summary of scores over all candidates
    #summary of scores over all candidates
    cmpd_info$cand_CIDs <- ""
    cmpd_info$cand_Scores <- ""
    cmpd_info$cand_NoExplPeaks <- ""
    cmpd_info$cand_FragmenterScore <- ""
    #MoNA
    #cmpd_info$cand_MetFusion <- ""
    cmpd_info$cand_MoNAIndiv <- ""
    #PubChem data 
    cmpd_info$cand_PubMedCount <- ""
    cmpd_info$cand_PatentCount <- ""
    cmpd_info$cand_AnnoCount <- ""
    # extra annotation terms
    if (length(grep("agro",extra_terms))>0) {
        cmpd_info$cand_AgroChemInfo <- ""
    }
    if (length(grep("bio",extra_terms))>0) {
        cmpd_info$cand_BioPathway <- ""
    }
    if (length(grep("drug",extra_terms))>0) {
        cmpd_info$cand_DrugMedicInfo <- ""
    }
    if (length(grep("food",extra_terms))>0) {
        cmpd_info$cand_FoodRelated <- ""
    }
    if (length(grep("pharma",extra_terms))>0) {
        cmpd_info$cand_PharmacoInfo <- ""
    }
    if (length(grep("safety",extra_terms))>0) {
        cmpd_info$cand_SafetyInfo <- ""
    }
    if (length(grep("toxicity",extra_terms))>0) {
        cmpd_info$cand_ToxicityInfo <- ""
    }
    if (length(grep("use",extra_terms))>0) {
        cmpd_info$cand_KnownUse <- ""
    }
    
    if (IKmatch) {
        #TopCandMatch?
        cmpd_info$IKeyFBlock_match <- ""
        cmpd_info$IKeyFBlock_rank <- ""
    }
    
    return(cmpd_info)
}


# #LocalCSVScoreTerms <- "PubMed_Count,Patent_Count,FPSum"
# #LocalCSVScoreWeights <- ",1,1,1"
# 
# 
# user_loc_score_terms <- function(AnnoCount="AnnoTypeCount") {
#     list("PubMed_Count",
#          "Patent_Count",
#          "AnnoTypeCount")
# }
# 
# 
# user_loc_score_weights <- function() {
#     list(PubMed_Count=1,
#          Patent_Count=1,
#          AnnoTypeCount=1)
# }

# user_res_fn_suff <- function(i,cmpd_info) {
#     paste0(cmpd_info$ID[i],"_", cmpd_info$tag[i], "_",cmpd_info$set[i], "_",cmpd_info$adduct_type[i])
#     
# }


user_fill_cmpd_info <- function(mf_res,cmpd_info_row,IKmatch=TRUE,IK="",
                                extra_terms=c("none")) {
    # set up the extra terms
    test_terms <- grep(TRUE, extra_terms %in% c("agro","bio","drug","food", "pharma", 
                                                "safety", "toxicity", "use","none"))
    if(length(test_terms)<1) {
        stop("Incorrect extra terms: select one or more of agro, bio, drug, 
             food, pharma, safety, toxicity, use, none")
    }
    #find indexes of relevant scores
    index_maxScore <- 1 #which(max(as.numeric(mf_res$Score))==as.numeric(mf_res$Score))
    max_MoNAIndiv <- max(as.numeric(MetFrag_res$OfflineIndividualMoNAScore))
    if (max_MoNAIndiv>0) {
        index_maxMoNAScore <- which(max_MoNAIndiv==as.numeric(MetFrag_res$OfflineIndividualMoNAScore))
    } else {
        index_maxMoNAScore <- 0
    }
    
    cmpd_info_row <- as.list(cmpd_info_row)
    ## Number of candidates and score summaries
    #cmpd_info_row$msms_peaks <- mf_res$NumberPeaksUsed[index_maxScore]
    cmpd_info_row$num_poss_IDs <- length(mf_res$Score)
    cmpd_info_row$max_Score <- max(as.numeric(mf_res$Score))
    cmpd_info_row$n_Score_GE4 <- length(which(as.numeric(mf_res$Score)>=4))
    #cmpd_info_row$n_Score_GE3 <- length(which(as.numeric(mf_res$Score)>=3))
    cmpd_info_row$n_Score_GE2 <- length(which(as.numeric(mf_res$Score)>=2))
    #basic metadata summary
    n_MoNA_GEp9 <- length(which(as.numeric(MetFrag_res$OfflineIndividualMoNAScore)>=0.9))
    cmpd_info_row$n_MoNA_GEp9	<- length(which(as.numeric(MetFrag_res$OfflineIndividualMoNAScore)>=0.9))
    #cmpd_info$n_DrugInfo_cand[i] <- length(which(as.numeric(MetFrag_res$DrugMedicInfo)>=1))
    #cmpd_info$n_FoodRel_cand[i] <- length(which(as.numeric(MetFrag_res$FoodRelated)>=1))
    #assign auto level 
    if ((max_MoNAIndiv>=0.9) && (n_MoNA_GEp9==1)) {
        cmpd_info_row$AutoLevel <- "2a"
        # Top Cand is the MoNA match
        index_topCand <- index_maxMoNAScore
    } else if (max_MoNAIndiv>=0.9) {
        cmpd_info_row$AutoLevel <- "3"
        # Top Cand is the highest MoNA match but at level 3
        index_topCand <- index_maxMoNAScore
    } else {
        index_maxMoNAScore <- 0
        cmpd_info_row$AutoLevel <- "3"
        # Top Cand is one with the top score
        index_topCand <- index_maxScore
    }
    
    ## Summary of top candidate and max metadata values 
    # maximum score
    cmpd_info_row$CID_maxScore <- mf_res$Identifier[index_maxScore]
    cmpd_info_row$SMILES_maxScore <- mf_res$SMILES[index_maxScore]
    cmpd_info_row$InChIKey_maxScore <- mf_res$InChIKey[index_maxScore]
    #cmpd_info_row$Name_maxScore <- mf_res$CompoundName[index_maxScore]
    cmpd_info_row$Name_maxScore <- mf_res$Synonym[index_maxScore]
    cmpd_info_row$ExplPeaks_maxScore <- mf_res$ExplPeaks[index_maxScore]
    # Top candidate
    cmpd_info_row$SMILES_TopCand <- mf_res$SMILES[index_topCand]
    cmpd_info_row$Name_TopCand <- mf_res$Synonym[index_topCand]
    cmpd_info_row$CID_TopCand <- mf_res$Identifier[index_topCand]
    cmpd_info_row$ExplPeaks_TopCand <- mf_res$ExplPeaks[index_topCand]
    cmpd_info_row$MoNA_TopCand <- mf_res$OfflineIndividualMoNAScore[index_topCand]
    
    #match?
    if (IKmatch) {
        IKeyFBlock_rank <- grep(strtrim(IK,14),mf_res$FirstBlock)
        if (length(IKeyFBlock_rank) > 0) {
            cmpd_info_row$RankSuspect <- IKeyFBlock_rank
            if (IKeyFBlock_rank==index_topCand) {
                cmpd_info_row$TopCandIsSuspect <- TRUE
            } else {
                cmpd_info_row$TopCandIsSuspect <- FALSE
            }
        } else {
            cmpd_info_row$RankSuspect <- NA
            cmpd_info_row$TopCandIsSuspect <- FALSE
        }
    }

    
    ## Max of individual categories
    ## MetFrag
    cmpd_info_row$max_NoExplPeaks <- max(as.numeric(mf_res$NoExplPeaks))
    cmpd_info_row$NumberPeaksUsed <- max(as.numeric(mf_res$NumberPeaksUsed))
    cmpd_info_row$max_FragmenterScore <- max(as.numeric(mf_res$FragmenterScore))
    ## MoNA
    #cmpd_info_row$max_MetFusion <- max(as.numeric(mf_res$OfflineMetFusionScore))
    cmpd_info_row$max_MoNAIndiv <- max(as.numeric(mf_res$OfflineIndividualMoNAScore))
    #PubChem data
    cmpd_info_row$max_PubMedCount <- suppressWarnings(max(as.numeric(mf_res$PubMed_Count),na.rm=T))
    cmpd_info_row$max_PatentCount <- suppressWarnings(max(as.numeric(mf_res$Patent_Count),na.rm=T))
    cmpd_info_row$max_AnnoCount <- suppressWarnings(max(as.numeric(mf_res$AnnoTypeCount),na.rm=T))

    ## Summary of scores over all candidates
    cmpd_info_row$cand_CIDs <- paste(mf_res$Identifier,collapse=";")
    cmpd_info_row$cand_Scores <- paste(mf_res$Score,collapse=";")
    cmpd_info_row$cand_NoExplPeaks <- paste(mf_res$NoExplPeaks,collapse=";")
    cmpd_info_row$cand_FragmenterScore <- paste(mf_res$FragmenterScore,collapse=";")
    ## MoNA
    #cmpd_info_row$cand_MetFusion <- paste(mf_res$OfflineMetFusionScore,collapse=";")
    cmpd_info_row$cand_MoNAIndiv <- paste(mf_res$OfflineIndividualMoNAScore,collapse=";")
    ## PubChem data
    cmpd_info_row$cand_PubMedCount <- paste(mf_res$PubMed_Count,collapse=";")
    cmpd_info_row$cand_PatentCount <- paste(mf_res$Patent_Count,collapse=";")
    cmpd_info_row$cand_AnnoCount <- paste(mf_res$AnnoTypeCount,collapse=";")

    # extra terms
    # AgroChemInfo, BioPathway, DrugMedicInfo, FoodRelated, 
    # PharmacoInfo, SafetyInfo, ToxicityInfo, KnownUse
    # NOTE: BioPathway is tier1 only
    
    if (length(grep("agro",extra_terms))>0) {
        cmpd_info_row$max_AgroChemInfo <- suppressWarnings(max(as.numeric(mf_res$AgroChemInfo),na.rm=T))
        cmpd_info_row$cand_AgroChemInfo <- paste(mf_res$AgroChemInfo,collapse=";")
    }
    if (length(grep("bio",extra_terms))>0) {
        cmpd_info_row$max_BioPathway <- suppressWarnings(max(as.numeric(mf_res$BioPathway),na.rm=T))
        cmpd_info_row$cand_BioPathway <- paste(mf_res$BioPathway,collapse=";")
    }
    if (length(grep("drug",extra_terms))>0) {
        cmpd_info_row$max_DrugMedicInfo <- suppressWarnings(max(as.numeric(mf_res$DrugMedicInfo),na.rm=T))
        cmpd_info_row$cand_DrugMedicInfo <- paste(mf_res$DrugMedicInfo,collapse=";")
    }
    if (length(grep("food",extra_terms))>0) {
        cmpd_info_row$max_FoodRelated <- suppressWarnings(max(as.numeric(mf_res$FoodRelated),na.rm=T))
        cmpd_info_row$cand_FoodRelated <- paste(mf_res$FoodRelated,collapse=";")
    }
    if (length(grep("pharma",extra_terms))>0) {
        cmpd_info_row$max_PharmacoInfo <- suppressWarnings(max(as.numeric(mf_res$PharmacoInfo),na.rm=T))
        cmpd_info_row$cand_PharmacoInfo <- paste(mf_res$PharmacoInfo,collapse=";")
    }
    if (length(grep("safety",extra_terms))>0) {
        cmpd_info_row$max_SafetyInfo <- suppressWarnings(max(as.numeric(mf_res$SafetyInfo),na.rm=T))
        cmpd_info_row$cand_SafetyInfo <- paste(mf_res$SafetyInfo,collapse=";")
    }
    if (length(grep("toxicity",extra_terms))>0) {
        cmpd_info$max_ToxicityInfo <- suppressWarnings(max(as.numeric(mf_res$ToxicityInfo),na.rm=T))
        cmpd_info$cand_ToxicityInfo <- paste(mf_res$ToxicityInfo,collapse=";")
    }
    if (length(grep("use",extra_terms))>0) {
        cmpd_info_row$max_KnownUse <- suppressWarnings(max(as.numeric(mf_res$KnownUse),na.rm=T))
        cmpd_info_row$cand_KnownUse <- paste(mf_res$KnownUse,collapse=";")
    }
    

    return(cmpd_info_row)
    
}

# a function to reduce compound info to those with MS/MS (if MS2only=T)
# and unique by mz (not checking RT at all)
ftable_select.mz <- function(ftable_cmpd_info,MS2only=TRUE) {
    if (MS2only) {
        cmpd_info <- cmpd_info[which(cmpd_info$MS2==TRUE),]
        cmpd_info <- cmpd_info[which(!is.na(cmpd_info$MS2rt)),]
    }
#    length(unique(cmpd_info$mz))
    suspect_mz <- unique(cmpd_info$mz)
    #  cull by mz ... 
#    i <- 1
    i_mzs <- vector(length=length(suspect_mz))
    for (i in 1:length(suspect_mz)) {
        i_mz <- grep(suspect_mz[i],cmpd_info$mz)[1]
        i_mzs[i] <- i_mz
    }
    # trim to have only one entry per name
    cmpd_info <- cmpd_info[i_mzs,]
    # there are some NAs .. 
    mz_na <- which(is.na(cmpd_info$mz))
    # remove these NAs ... 
    if (length(mz_na)>0) {
        cmpd_info <- cmpd_info[-mz_na,]
    }
    return(cmpd_info)
}


# a function to reduce compound info to those with MS/MS (if MS2only=T)
# and unique by Name (not checking RT at all). 
ftable_select.name <- function(ftable_cmpd_info,MS2only=TRUE) {
    if (MS2only) {
        cmpd_info <- cmpd_info[which(cmpd_info$MS2==TRUE),]
        cmpd_info <- cmpd_info[which(!is.na(cmpd_info$MS2rt)),]
    }
    #    length(unique(cmpd_info$mz))
    suspect_names <- unique(cmpd_info$Name)
    #  cull by name ... 
    #    i <- 1
    i_names <- vector(length=length(suspect_names))
    for (i in 1:length(suspect_names)) {
        i_name <- grep(suspect_names[i],cmpd_info$Name)[1]
        i_names[i] <- i_name
    }
    # trim to have only one entry per name
    cmpd_info <- cmpd_info[i_names,]
    # there are some NAs .. 
    name_na <- which(is.na(cmpd_info$Name))
    # remove these NAs ... 
    if (length(name_na)>0) {
        cmpd_info <- cmpd_info[-name_na,]
    }
    return(cmpd_info)
}

# a function to reduce compound info to those with MS/MS (if MS2only=T)
# and unique by ID . 
ftable_select.ID <- function(cmpd_info,MS2only=TRUE) {
    if (MS2only) {
        cmpd_info <- cmpd_info[which(cmpd_info$MS2==TRUE),]
        cmpd_info <- cmpd_info[which(!is.na(cmpd_info$MS2rt)),]
    }
    #    length(unique(cmpd_info$mz))
    suspect_names <- unique(cmpd_info$ID)
    #  cull by name ... 
    #    i <- 1
    i_names <- vector(length=length(suspect_names))
    for (i in 1:length(suspect_names)) {
        i_name <- grep(suspect_names[i],cmpd_info$ID)[1]
        i_names[i] <- i_name
    }
    # trim to have only one entry per name
    cmpd_info <- cmpd_info[i_names,]
    # there are some NAs .. 
    name_na <- which(is.na(cmpd_info$ID))
    # remove these NAs ... 
    if (length(name_na)>0) {
        cmpd_info <- cmpd_info[-name_na,]
    }
    return(cmpd_info)
}

