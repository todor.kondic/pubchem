# Evaluation Script for PubChemLite
# Emma Schymanski, 18 Nov 2019
# Revised 11 May 2020-Jun 13 2020
# Trimming to PCLite benchmark only for Todor

###### Set up packages & folders ######

# Set up packages
# if (!requireNamespace("BiocManager", quietly = TRUE))
#   install.packages("BiocManager")
# BiocManager::install()

# ##If you don't have the latest RChemMass, reinstall:
# library(devtools)
# install_github("MassBank/RMassBank",dependencies=F)
# install_github("schymane/RChemMass", dependencies = F)
# install_github("schymane/ReSOLUTION", dependencies = F)
library(devtools)
library(RMassBank)
library(ReSOLUTION)
library(RChemMass)

# set up folders
base_dir <- "C:/DATA/PubChem/PubChemLite/Evaluation/"
setwd(base_dir)
db_dir <- "C:/CompoundDBs/MetFrag_CSVs/"
metfrag_dir <- "C:/DATA/MetFrag/"
# get user_PCLite_eval.R file
user_file_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/pubchemlite/R/user_PCLite_eval.R?inline=false"
download.file(user_file_url,paste0(base_dir,"user_PCLite_eval.R"))
source(paste0(base_dir,"user_PCLite_eval.R"))
# get PCLite_eval_support.R file
user_file_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/pubchemlite/R/PCLite_eval_support.R?inline=false"
download.file(user_file_url,paste0(base_dir,"PCLite_eval_support.R"))
source(paste0(base_dir,"PCLite_eval_support.R"))
# get extractAnnotations.R file
extr_anno_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/annotations/tps/extractAnnotations.R?inline=false"
download.file(extr_anno_url,paste0(base_dir,"extractAnnotations.R"))
source(paste0(base_dir,"extractAnnotations.R"))



#### Evaluation for Benchmarking set - PubChemLite ####

## First step (run once only): creating the benchmarking file
## see separate script ...PubChemLiteEval_MetFragRL_Merged.R

## Evaluations
# pre MSMS:
#cmpd_file <- paste0(base_dir,"PCLite_EvalSets_mergedPCIDs_wBenchmark.csv")
#msms_dir <- paste0(base_dir,"MSMS/") #not relevant

### 14 Jan, tier 1 
localDB <- paste0(db_dir,"PubChemLite_14Jan2020_tier1.csv")
file.exists(localDB)
# post MSMS being added to file:
cmpd_file <- paste0(base_dir, "PCLite_Benchmark_wMSMS_info.csv")
file.exists(cmpd_file)
# set up results
run_name <- "tier1_14Jan2020_MergedBenchmark"
pre2020 <- FALSE
useMSMS <- TRUE

cmpd_info <- read.csv(cmpd_file,stringsAsFactors = F)
cmpd_info <- user_mk_cmpd_info(cmpd_info,skipMSMS = T)

runPCLiteEval.BM(cmpd_info,localDB,run_name,metfrag_dir,useMSMS=TRUE,
                 pre2020=FALSE,base_dir=base_dir,verbose=TRUE,runMetFrag=TRUE)

## 14 Jan, tier0
localDB <- paste0(db_dir,"PubChemLite_14Jan2020_tier0.csv")
file.exists(localDB)
cmpd_file <- paste0(base_dir, "PCLite_Benchmark_wMSMS_info.csv")
file.exists(cmpd_file)
# set up results
run_name <- "tier0_14Jan2020_MergedBenchmark"
pre2020 <- FALSE
useMSMS <- TRUE

cmpd_info <- read.csv(cmpd_file,stringsAsFactors = F)
cmpd_info <- user_mk_cmpd_info(cmpd_info,skipMSMS = T)

runPCLiteEval.BM(cmpd_info,localDB,run_name,metfrag_dir,useMSMS=TRUE,
                 pre2020=FALSE,base_dir=base_dir,verbose=TRUE,runMetFrag=TRUE)

# 18 Nov 2019, tier0
localDB <- paste0(db_dir,"PubChemLite_18Nov2019_tier0.csv")
file.exists(localDB)
cmpd_file <- paste0(base_dir, "PCLite_Benchmark_wMSMS_info.csv")
file.exists(cmpd_file)
# set up results
run_name <- "tier0_18Nov2019_MergedBenchmark"
pre2020 <- TRUE
useMSMS <- TRUE

cmpd_info <- read.csv(cmpd_file,stringsAsFactors = F)
cmpd_info <- user_mk_cmpd_info(cmpd_info,skipMSMS = T)

runPCLiteEval.BM(cmpd_info,localDB,run_name,metfrag_dir,useMSMS,pre2020,
                 base_dir=base_dir,verbose=TRUE,runMetFrag=TRUE)

## 22 May 2020, tier0
localDB <- paste0(db_dir,"PubChemLite_tier0_20200522.csv")
file.exists(localDB)
cmpd_file <- paste0(base_dir, "PCLite_Benchmark_wMSMS_info.csv")
file.exists(cmpd_file)
# set up results
run_name <- "tier0_22May2020_MergedBenchmark"
pre2020 <- FALSE
useMSMS <- TRUE

cmpd_info <- read.csv(cmpd_file,stringsAsFactors = F)
cmpd_info <- user_mk_cmpd_info(cmpd_info,skipMSMS = T)

runPCLiteEval.BM(cmpd_info,localDB,run_name,metfrag_dir,useMSMS,pre2020,
                 base_dir=base_dir,verbose=TRUE,runMetFrag=TRUE)

# 12 June 2020, tier0
localDB <- paste0(db_dir,"PubChemLite_tier0_20200612.csv")
file.exists(localDB)
# post MSMS:
cmpd_file <- paste0(base_dir, "PCLite_Benchmark_wMSMS_info.csv")
file.exists(cmpd_file)
# set up results
run_name <- "tier0_12Jun2020_MergedBenchmark"
pre2020 <- FALSE
useMSMS <- TRUE

cmpd_info <- read.csv(cmpd_file,stringsAsFactors = F)
cmpd_info <- user_mk_cmpd_info(cmpd_info,skipMSMS = T)

# # testing briefly:
# runPCLiteEval.BM(cmpd_info[1:3,],localDB,run_name,metfrag_dir,useMSMS,pre2020,
#                  base_dir=base_dir,verbose=TRUE,runMetFrag=TRUE)
# full run
runPCLiteEval.BM(cmpd_info,localDB,run_name,metfrag_dir,useMSMS,pre2020,
                 base_dir=base_dir,verbose=TRUE,runMetFrag=TRUE)


##### Supporter Functions #####
# Ported to PCLite_eval_support.R
