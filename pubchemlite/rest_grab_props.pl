#!/usr/bin/perl

use strict;
use warnings;

my $bsize = 300;

my $url_prefix = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/";
my $url_suffix = "/property/MolecularFormula,InChIKey,IsomericSMILES,InChI,MonoisotopicMass,IUPACName/JSON";

#  print "$pid\t$i\t$fp\t$npmid\t$nppid\t$cids\n";

my $fp_len = 0;
my $is_first = 1;
my %u = ();

my $legendfile = "$ARGV[0]";
print STDERR ":: Using legend file: \"$legendfile\" ::\n";


my %map = ();
my @hdrl = ();
open(FMAP,$legendfile);
while ($_ = <FMAP>) {
    chop;
    my ($cty,$mfcol) = split(/\s*,\s*/,$_,2);
    push @hdrl,$mfcol;

}
my $hdr = join("",join("\t",@hdrl),"\n");

print STDERR ":: Header is $hdr\n";
    
while ( $_ = <STDIN> ) {
  chop;
  my ( $pcid, @tmp ) = split( /	/, $_, 4 );
  if ( $is_first == 1 ) {
    $is_first = 0;
    $fp_len = length( $tmp[1] );
  }

  $u{ $pcid } = $_;
}

print "Identifier\tFirstBlock\tFP\tPubMed_Count\tPatent_Count\tRelated_CIDs",
      "\tSynonym\tMolecularFormula\tSMILES\tInChI\tInChIKey\tMonoisotopicMass",
      "\tCompoundName\tAnnoTypeCount",
      "\t$hdr";



my %v = ();
my @cids = sort bynum( keys( %u ) );
my $ncid = @cids;
my $nbatch = 1 + int( ( $ncid - 1 ) / $bsize );
print STDERR "Read $ncid records\nProcessing $nbatch batches @ $bsize CIDs at a time\n";

my $extra = "";
my $cid = 0;
my $mf = "";
my $is = "";
my $in = "";
my $ik = "";
my $im = "";
my $iu = "";
my $fnamelog = "tmp_" . $$ . ".log";
my $fnamejson = "tmp_" . $$ . ".json";
for ( my $ibatch = 0;  $ibatch < $nbatch;  $ibatch++ ) {
  print STDERR "Working on batch ", ( $ibatch + 1 ), "\n";
  my $offset = $ibatch * $bsize;

  my $n = $offset + $bsize;
  if ( $ncid < $n ) {
    $n = $ncid;
  }

  my $cid_found = 0;
  my @bcids = ();
  for ( my $i = $offset;  $i < $n;  $i++ ) {
    push( @bcids, $cids[ $i ] );
  }

  my $lcid = join( ",", @bcids );
  my $nbcids = @bcids;
#  print STDERR "Range:  $offset .. $n .. $nbcids .. $ibatch of $nbatch.. $bsize .. $ncid\n$lcid\n";
  print STDERR "Range:  $offset .. $n .. $nbcids .. ", ( $ibatch + 1 ), " of $nbatch.. $bsize .. $ncid\n";

  my $url = $url_prefix . $lcid . $url_suffix;
  my $command = "/usr/bin/wget --header=\"accept-encoding: gzip\" " . $url . " -O " . $fnamejson . " -o " . $fnamelog;
#  print STDERR "$command\n";
  print `$command\n`;
  sleep( 1 );
  print STDERR `/usr/bin/cat $fnamelog`;

  $cid = 0;
  $mf = "";
  $is = "";
  $in = "";
  $ik = "";
  $im = "";
  $iu = "";
  open( TMP, "/usr/bin/gunzip < $fnamejson |" ) || die "Unable to read $fnamejson file!\n";
  while ( $_ = <TMP> ) {
    if ( $_ =~ /\"CID\"/ ) {
      if ( $cid != 0 && defined( $u{ $cid } ) ) {
        if ( defined( $v{ $cid } ) ) {
          print STDERR "CID $cid appears more than once!\n";
        } else {
          $v{ $cid } = "";
          my @t = split( /	/, $u{$cid} );
          if ( length( $t[2] ) != $fp_len ) {
            print STDERR "Fingerprint for CID $cid is corrupt .. length is ", length( $t[2] ), " != $fp_len\n";
          }
          my @bits = splice( @t, 3, $fp_len + 1 );
          my $nt = @t;
          if ( $nt == 6 ) {
            push( @t, "" );  # Missing chemical name
          }
          print join( "\t", @t ), "\t$mf\t$is\t$in\t$ik\t$im\t$iu\t", join( "\t", @bits ), "\n";
          $cid_found++;
        }
      }

      $_ =~ /\"CID\": (.+),/;
      $cid = $1;
      $mf = "";
      $is = "";
      $in = "";
      $ik = "";
      $im = "";
      $iu = "";
    } elsif ( $_ =~ /\"MolecularFormula\"/ ) {
      $_ =~ /\"MolecularFormula\": \"(.+)\",/;
      $mf = $1;
    } elsif ( $_ =~ /\"IsomericSMILES\"/ ) {
      $_ =~ /\"IsomericSMILES\": \"(.+)\",/;
      $is = $1;
    } elsif ( $_ =~ /\"InChI\"/ ) {
      $_ =~ /\"InChI\": \"(.+)\",/;
      $in = $1;
    } elsif ( $_ =~ /\"InChIKey\"/ ) {
      $_ =~ /\"InChIKey\": \"(.+)\",/;
      $ik = $1;
    } elsif ( $_ =~ /\"MonoisotopicMass\"/ ) {
      $_ =~ /\"MonoisotopicMass\": (.+)/;
      $im = $1;
    } elsif ( $_ =~ /\"IUPACName\"/ ) {
      $_ =~ /\"IUPACName\": \"(.+)\"/;
      $iu = $1;
    }
  }
  close( TMP );

      if ( $cid != 0 && defined( $u{ $cid } ) ) {
        if ( defined( $v{ $cid } ) ) {
          print STDERR "CID $cid appears more than once!\n";
        } else {
          $v{ $cid } = "";
          my @t = split( /	/, $u{$cid} );
          if ( length( $t[2] ) != $fp_len ) {
            print STDERR "Fingerprint for CID $cid is corrupt .. length is ", length( $t[2] ), " != $fp_len\n";
          }
          my @bits = splice( @t, 3, $fp_len + 1 );
          my $nt = @t;
          if ( $nt == 6 ) {
            push( @t, "" );  # Missing chemical name
          }
          print join( "\t", @t ), "$extra\t$mf\t$is\t$in\t$ik\t$im\t$iu\t", join( "\t", @bits ), "\n";
        }
        $cid_found++;
      }

  if ( $cid_found != ( $n - $offset ) ) {
    print STDERR "WARNING :: only $cid_found CID found out of ", ( $n - $offset ), "\n";
  }
}

my @mc = ();
foreach my $c ( @cids ) {
  if ( ! defined( $v{ $c } ) ) {
    print STDERR "Missing from output CID $c!\n";
    push( @mc, $c );
  }
}

@cids = @mc;
$ncid = @cids;
if ( $ncid == 0 ) { print STDERR `/bin/rm $fnamelog $fnamejson`;  exit( 0 ); }


print STDERR "There are $ncid missing CIDs\n\n";

# Try one more time to get the missing CID info
$bsize /= 2;  # Try a smaller batch size
$nbatch = 1 + int( ( $ncid - 1 ) / $bsize );
print STDERR "Retry .. Read $ncid records\nProcessing $nbatch batches @ $bsize CIDs at a time\n";

for ( my $ibatch = 0;  $ibatch < $nbatch;  $ibatch++ ) {
  print STDERR "Working on batch ", ( $ibatch + 1 ), "\n";
  my $offset = $ibatch * $bsize;

  my $n = $offset + $bsize;
  if ( $ncid < $n ) {
    $n = $ncid;
  }

  my @bcids = ();
  for ( my $i = $offset;  $i < $n;  $i++ ) {
    push( @bcids, $cids[ $i ] );
  }

  my $lcid = join( ",", @bcids );
  my $nbcids = @bcids;
#  print STDERR "Range:  $offset .. $n .. $nbcids .. $ibatch of $nbatch.. $bsize .. $ncid\n$lcid\n";
  print STDERR "Range:  $offset .. $n .. $nbcids .. $ibatch of $nbatch.. $bsize .. $ncid\n";

  my $url = $url_prefix . $lcid . $url_suffix;
#  my $command = "/usr/bin/wget " . $url . " -O " . $fnamejson . " -o " . $fnamelog;
  my $command = "/usr/bin/wget -S --header=\"accept-encoding: gzip\" " . $url . " -O " . $fnamejson . " -o " . $fnamelog;
#  print STDERR "$command\n";
  print `$command\n`;
  sleep( 1 );
  print STDERR `/usr/bin/cat $fnamelog`;

  $cid = 0;
  $mf = "";
  $is = "";
  $in = "";
  $ik = "";
  $im = "";
  $iu = "";
  open( TMP, "/usr/bin/gunzip < $fnamejson |" ) || die "Unable to read $fnamejson file!\n";
  while ( $_ = <TMP> ) {
    if ( $_ =~ /\"CID\"/ ) {
      if ( $cid != 0 && defined( $u{ $cid } ) ) {
        if ( defined( $v{ $cid } ) ) {
          print STDERR "CID $cid appears more than once!\n";
        } else {
          $v{ $cid } = "";
          my @t = split( /	/, $u{$cid} );
          if ( length( $t[2] ) != $fp_len ) {
            print STDERR "Fingerprint for CID $cid is corrupt .. length is ", length( $t[2] ), " != $fp_len\n";
          }
          my @bits = splice( @t, 3, $fp_len + 1 );
          my $nt = @t;
          if ( $nt == 6 ) {
            push( @t, "" );  # Missing chemical name
          }
          print join( "\t", @t ), "$extra\t$mf\t$is\t$in\t$ik\t$im\t$iu\t", join( "\t", @bits ), "\n";
#          my @t = split( "	", $u{$cid}, 4 );
#          my $fp = $t[2];
#          if ( length( $fp ) != $fp_len ) {
#            print STDERR "Fingerprint for CID $cid is corrupt .. length is ", length( $fp ), " != $fp_len\n";
#          }
#          my @bits = ();
#          for ( my $i = 0; $i < $fp_len; $i++ ) {
#            push( @bits, substr( $fp, $i, 1 ) );
#          }
#          print $u{$cid}, "\t$mf\t$is\t$in\t$ik\t$im\t$iu\t", join( "\t", @bits ), "\n";
        }
      }

      $_ =~ /\"CID\": (.+),/;
      $cid = $1;
    } elsif ( $_ =~ /\"MolecularFormula\"/ ) {
      $_ =~ /\"MolecularFormula\": \"(.+)\",/;
      $mf = $1;
    } elsif ( $_ =~ /\"IsomericSMILES\"/ ) {
      $_ =~ /\"IsomericSMILES\": \"(.+)\",/;
      $is = $1;
    } elsif ( $_ =~ /\"InChI\"/ ) {
      $_ =~ /\"InChI\": \"(.+)\",/;
      $in = $1;
    } elsif ( $_ =~ /\"InChIKey\"/ ) {
      $_ =~ /\"InChIKey\": \"(.+)\",/;
      $ik = $1;
    } elsif ( $_ =~ /\"MonoisotopicMass\"/ ) {
      $_ =~ /\"MonoisotopicMass\": (.+)/;
      $im = $1;
    } elsif ( $_ =~ /\"IUPACName\"/ ) {
      $_ =~ /\"IUPACName\":  \"(.+)\"/;
      $iu = $1;
    }
  }
  close( TMP );

      if ( $cid != 0 && defined( $u{ $cid } ) ) {
        if ( defined( $v{ $cid } ) ) {
          print STDERR "CID $cid appears more than once!\n";
        } else {
          $v{ $cid } = "";
          my @t = split( /	/, $u{$cid} );
          if ( length( $t[2] ) != $fp_len ) {
            print STDERR "Fingerprint for CID $cid is corrupt .. length is ", length( $t[2] ), " != $fp_len\n";
          }
          my @bits = splice( @t, 3, $fp_len + 1 );
          my $nt = @t;
          if ( $nt == 6 ) {
            push( @t, "" );  # Missing chemical name
          }
          print join( "\t", @t ), "$extra\t$mf\t$is\t$in\t$ik\t$im\t$iu\t", join( "\t", @bits ), "\n";
        }
      }
}

@mc = ();
foreach my $c ( @cids ) {
  if ( ! defined( $v{ $c } ) ) {
    print STDERR "Missing from output CID $c!\n";
    push( @mc, $c );
  }
}

@cids = @mc;
$ncid = @cids;
if ( $ncid == 0 ) { print STDERR `/bin/rm $fnamelog $fnamejson`;  exit( 0 ); }

print STDERR "There are $ncid missing CIDs\n\n";
print STDERR `/bin/rm $fnamelog $fnamejson`;

sub bynum { $a <=> $b };

