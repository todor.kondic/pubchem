# INITIALISATION

# Logging.
export LOGFILE=$PWD/"$(date +%Y%m%d)_build.log"
[ -n "$LOGFILE" ] && exec 2> "$LOGFILE" && echo "********** PubChem Lite LOG START $(date) **********" 1>&2

# Kill top-level process using fatal from a subshell.
trap "exit 1" SIGUSR1
TOPPID="$$"
fatal(){
  echo "(FATAL!)" "$@" >&2
  kill -s SIGUSR1 "$TOPPID"
}

# Scripts location.
export SCRIPTDIR=$(dirname $(readlink -f "$BASH_SOURCE"))



# INPUTS

# Path to input directory.
export INPUTDIR="$1"
# Manifest file.
export MANIF="$INPUTDIR/manifest.yaml"

# INITIALISE ENVIRONMENT FROM MANIFEST

# Read in and declare variables from the manifest.
while read -r type key val; do
    [[ "$type" == "s" ]] && declare -x $key="$val" || \
	    [[ "$type" == "a" ]] && declare -x -a $key="$val"
done < <(perl "${SCRIPTDIR}/read_manifest.pl" "$MANIF")

# The map file (bits, categories, metfrag columns).
export MAPFILE=$(readlink -f "$INPUTDIR")/$MAPFILE

# Filter filename.
export FILTERFILE=$(basename "${MAPFILE%.map}.filter")

# Legend filename.
export LEGENDFILE=$(basename "${MAPFILE%.map}.legend")

# MetFrag filename.
export OUTMFFILE=$(basename "${MAPFILE%.map}_$(date +%Y%m%d).csv")
export OUTMFCLONE=$(basename "${MAPFILE%.map}.csv")


# Does manifest exist?
[ ! -e "$MANIF" ] && fatal "(pb-lite-driver.sh): Manifest file $MANIF not found, or unreadable."
# What about the filter file?
[ ! -e "$MAPFILE" ] && fatal "(pb-lite-driver): Error, the map file $MAPFILE does not exist."

# Top level project directory.
export TOPDIR=$(readlink -f "$TOPDIR")
mkdir -p "$TOPDIR"
[ ! -d "$TOPDIR" ] && fatal "(pb-lite-driver): Error. TOPDIR does not exist: $TOPDIR"

# Where to build.
export WORKDIR="$TOPDIR/build"
mkdir -p "$WORKDIR"

bwd=$(basename "$WORKDIR")
export OUTDIR="$OUTDIR/$bwd"



source "${SCRIPTDIR}/library.sh"



# GENERATE BIT AND LEGEND INPUTS


gen_filtfile "$MAPFILE" "$WORKDIR" "$FILTERFILE"
gen_legend "$MAPFILE" "$WORKDIR" "$LEGENDFILE"

FILTERFILE="$WORKDIR"/$(basename "$FILTERFILE")
LEGENDFILE="$WORKDIR"/$(basename "$LEGENDFILE")

# PREPARATION
cd "$TOPDIR"


# Write basic build info to the log file.
stamp

# Clean up.
rm -vf "$WORKDIR"/*.{log,out,rpt,csv}

# Take care of the backup, if there is what to backup.
[ -d "$OUTDIR" ] && rm -Rf "${OUTDIR}.bak"
[ -d "$OUTDIR" ] && mv "${OUTDIR}" "${OUTDIR}.bak"

# Download sources for the build.
(cd "$WORKDIR"; source "${SCRIPTDIR}/fetch_sources")

# Adapt paths.
(cd "$WORKDIR"; source "${SCRIPTDIR}/adapt_scripts")

# Is there anything obviously crazy with the sources?
(cd "$WORKDIR"; source "${SCRIPTDIR}/sanity_prebuild" "${OUTDIR}")

# BUILD
header BUILD
(cd "$WORKDIR"; source "${SCRIPTDIR}/generate" "$FILTERFILE" "$LEGENDFILE")
footer BUILD

# POST-BUILD CHECK

# Check CSVs after the build process.
header POST-BUILD CHECK
"${SCRIPTDIR}"/sanity_postbuild
footer POST-BUILD CHECK

# Evaluate the quality of the CSV.
header EVAL4PUB CHECK
(cd "$WORKDIR"; source "${SCRIPTDIR}/eval4pub.sh")
footer EVAL4PUB CHECK


# Dump WORKDIR to OUTDIR.
header BACK WORKDIR UP $(date) 
mkdir -p "$OUTDIR"
rsync -auz --log-file="${WORKDIR}/build.transfer.log"  "${WORKDIR}/" "$OUTDIR"
footer BACK WORKDIR UP $(date) 

# COMMIT METFRAG CSVs
header COMMIT METFRAG CSVs
for dr in "${MF_DIRS[@]}"; do
    mkdir -p $dr
    [ ! -d "$dr" ] && \
	fatal "(pb-lite-driver): Error. MetFrag CSV dir $dr does not exist."
    cp -va "$WORKDIR/$OUTMFFILE" "$dr" 1>&2
    cp -va "$WORKDIR/$OUTMFFILE" "$dr/$OUTMFCLONE" 1>&2
done
footer COMMIT METFRAG CSVs

[ -n "${LOGFILE}" ] && echo "********** PubChem Lite LOG END $(date) **********" 1>&2

cp -va "$LOGFILE" "$OUTDIR"
cp -va "$LOGFILE" "${MF_DIRS[0]}/build_log"
