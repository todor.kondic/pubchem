ColHeader	DisplayColHeader	Note	DisplayNote	Static/Updated
ID	ID			
CAS_RN	CAS_RN			
InChIKey	InChIKey			
SMILES	SMILES			
PubChem_CID	PubChem_CID			
ProtectedCAS	ProtectedCAS			
Name	Name			
Synonyms	Synonyms	Trade names are also included here if available.		
Molecular_Formula	Molecular Formula			
SMILES_originalTable	SMILES_originalTable	Simplified molecular-input line-entry system; NA = not available.		
Previous_CAS	Previous_CAS			
Structure_Category	Structure Category	"For details, see sheet 2_structure_categories"		
Structure_Category_Name	Structure Category Name			
Number_Functional_Groups	Number of Functional Groups	1 = mono; >1 = more than one functional group		
Perfluoroalkyl_Chain_Length	Perfluorocarbon Chain Length	"For polymers with unknown chain length, it is currently expressed as "">20"""		
Linear_vs_Branched_Isomer	Linear vs Branched Isomer	Linear = linear isomer; Branched = branched isomer		
Potential_precursor_to_PFAAs_in_environment_or_biota	Potential Precursor to PFAAs in Environment or Biota	Precursor or Non-precursor		
Polymer	Non-polymer vs Polymer	Polymer or Non-polymer		
Mixture	Single Compound vs Mixture	Mixture or Single Compound		
Related_Chemicals	Related Chemicals	CAS Number - the acid counterpart of the salts; structure code - the second functionality group		
Uncertainty_in_Structure_Categorisation	Uncertainty in Structure Categorisation	"1 = uncertainty; 
0 = certainty"		
Note	Note	"Note to the structure categorisation or to the substance in general. 
1-H = there is a hydrogen atom at one end of the perfluoroalkyl chain"		
Regulatory_Status	Regulatory Status (as of June 2018)	"An entry here indicates that this substance was listed in any regulatory databases incl. those listed here and others, as of June 2018. Note that regulations have changed since with the Stockholm Convention, and the number of regulated PFAS have increased as a result. Updates in progress. "		
KEMI	KEMI	the excel version of the list was kindly provided by the Swedish Chemicals Agency in February 2017; https://www.kemi.se/global/rapporter/2015/report-7-15-occurrence-and-use-of-highly-fluorinated-substances-and-alternatives.pdf	Provided by the Swedish Chemicals Agency (KEMI) in February 2017; https://www.kemi.se/global/rapporter/2015/report-7-15-occurrence-and-use-of-highly-fluorinated-substances-and-alternatives.pdf	static
Scifinder	Scifinder	"SciFinder (https://scifinder.cas.org); substances with C3F7C-, iso-C3F7C-, CF3O-, C2F5O-, C3F7O- or iso-C3F7O-substructures, with references and with commercial sources are included here, with the exception that substances with the perfluorinated carbon chain shorter than C3F7C- and CF3OCF2C- are not considered here."	"From SciFinder (https://scifinder.cas.org); substances with C3F7C-, iso-C3F7C-, CF3O-, C2F5O-, C3F7O- or iso-C3F7O-substructures, with references and with commercial sources are included here, with the exception that substances with the perfluorinated carbon chain shorter than C3F7C- and CF3OCF2C- are not considered here."	regularly updated (Accessed on 9 February 2017)
OECD_2007_List	OECD 2007 List	http://www.oecd.org/officialdocuments/publicdisplaydocumentpdf/?doclanguage=en&cote=env/jm/mono(2006)15	From OECD Report at http://www.oecd.org/officialdocuments/publicdisplaydocumentpdf/?doclanguage=en&cote=env/jm/mono(2006)15	static
Australian_AICS	Australian AICS	Australian Inventory of Chemical Substances; kindly provided by NICNAS on 3 October 2017	Australian Inventory of Chemical Substances; kindly provided by NICNAS on 3 October 2017	regularly updated (last accessed on 3 October 2017)
Australian_IMAP_Tier_2	Australian IMAP Tier 2	the Inventory Multi-tiered Assessment and Prioritisation (IMAP) assessed by NICNAS for the human health and environmental impacts of unassessed industrial chemicals listed on the Australian Inventory of Chemical Substances (AICS); https://www.nicnas.gov.au/chemical-information/imap-assessments/IMAP-assessmentspublic-comment	The Inventory Multi-tiered Assessment and Prioritisation (IMAP) assessed by NICNAS for the human health and environmental impacts of unassessed industrial chemicals listed on the Australian Inventory of Chemical Substances (AICS); https://www.nicnas.gov.au/chemical-information/imap-assessments/IMAP-assessmentspublic-comment	static
Canadian_DSL	Canadian DSL	Canadian Dosmetic Substances List; https://pollution-waste.canada.ca/substances-search/Substance?lang=en	Canadian Dosmetic Substances List; https://pollution-waste.canada.ca/substances-search/Substance?lang=en	regularly updated (last accessed on 3 October 2017)
Canada_PCTSR_2012	Canada PCTSR 2012	"Non-exhaustive list of chemical abstracts service registry numbers for substances subject to the Prohibition Of Certain Toxic Substances Regulations, 2012"	"Non-exhaustive list of chemical abstracts service registry numbers for substances subject to the Prohibition Of Certain Toxic Substances Regulations, 2012"	static / periodically updated
China_IECSC	China IECSC	Inventory of Existing Chemical Substances Produced or Imported in China; https://chemicalwatch.com/asiahub/15187/existing-chemical-substances-inventory-2016-revisionchina/	Inventory of Existing Chemical Substances Produced or Imported in China; https://chemicalwatch.com/asiahub/15187/existing-chemical-substances-inventory-2016-revisionchina/	periodicaly updated (last accessed on 29 November 2017)
Japan_ENCS	Japan ENCS	Japanese Existing and New Chemical Substances Inventory (ENCS): http://www.nite.go.jp/en/chem/chrip/chrip_search/systemTop	Japanese Existing and New Chemical Substances Inventory (ENCS): http://www.nite.go.jp/en/chem/chrip/chrip_search/systemTop	static
Japan_Examples_of_PFOA_Stockholm_Convention	Japan Examples of PFOA Stockholm Convention	Examples of PFOA and related substances based on the discussion of POPRC12 under the Stockholm Convention; prepared for domestic survey by the Japanese METI; http://www.meti.go.jp/policy/chemical_management/int/2_besshi2.pdf	Examples of PFOA and related substances based on the discussion of POPRC12 under the Stockholm Convention; prepared for domestic survey by the Japanese METI; http://www.meti.go.jp/policy/chemical_management/int/2_besshi2.pdf	static
EU_REACH_Pre-registered	EU REACH Pre-registered	"used the version with the last update on 10/05/2016; used ""fluoro"" as the key word; https://echa.europa.eu/information-on-chemicals/pre-registered-substances"	"Source: EU/ECHA, searched for keyword ""fluoro"" on 10/05/2016 update at https://echa.europa.eu/information-on-chemicals/pre-registered-substances"	regularly updated (last accessed on 29 November 2017)
EU_REACH_Registered	EU REACH Registered	"used the version with the last update on 20/01/2017; used ""fluoro"" as the key word; https://echa.europa.eu/information-on-chemicals/registered-substances"	"Source: EU/ECHA, searched for keyword ""fluoro"" on 20/01/2017 update at https://echa.europa.eu/information-on-chemicals/registered-substances"	regularly updated (last accessed on 3 October 2017)
SPIN	SPIN	Substances in Preparations in Nordic Countries (SPIN) database; http://spin2000.net	Substances in Preparations in Nordic Countries (SPIN) database; http://spin2000.net	regularly updated (last accessed on 29 November 2017)
US_EPA_TSCA_Inventory	US EPA TSCA Inventory	US Toxic Substances Control Act (TSCA) Inventory: The non-confidential portion of US EPA's Toxic Substances Control Act Chemical Substance Inventory; https://www.epa.gov/tsca-inventory/how-access-tsca-inventory	US Toxic Substances Control Act (TSCA) Inventory: The non-confidential portion of US EPA's Toxic Substances Control Act Chemical Substance Inventory; https://www.epa.gov/tsca-inventory/how-access-tsca-inventory	regularly updated (last accessed of the June 2017 version)
US_EPA_IUR_1986-2002	US EPA IUR 1986-2002	"US EPA IUR (now CDR) database for the years between 1986 and 2002; previously downloaded from the US EPA website, and the link is no longer provided"	US EPA IUR (now CDR) database for the years between 1986 and 2002; previously downloaded from the US EPA website (link is no longer provided)	static
US_EPA_IUR_2006	US EPA IUR 2006	US EPA IUR (now CDR) database for the year 2006; https://www.epa.gov/chemical-data-reporting/downloadable-2006-iur-public-database	US EPA IUR (now CDR) database for the year 2006; https://www.epa.gov/chemical-data-reporting/downloadable-2006-iur-public-database	static
US_EPA_CDR_2012	US EPA CDR 2012	US EPA CDR (formerly IUR) database for the year 2012; https://www.epa.gov/chemical-data-reporting/2012-chemical-data-reporting-results	US EPA CDR (formerly IUR) database for the year 2012; https://www.epa.gov/chemical-data-reporting/2012-chemical-data-reporting-results	static
US_EPA_CDR_2016	US EPA CDR 2016	US EPA CDR (formerly IUR) database for the year 2016; https://www.epa.gov/chemical-data-reporting	US EPA CDR (formerly IUR) database for the year 2016; https://www.epa.gov/chemical-data-reporting	static
US_FDA_FCS	US FDA FCS	US FDA Inventory of Effective Food Contact Substances (FCS) Notifications; https://www.accessdata.fda.gov/scripts/fdcc/?set=FCN	US FDA Inventory of Effective Food Contact Substances (FCS) Notifications; https://www.accessdata.fda.gov/scripts/fdcc/?set=FCN	regularly updated (last accessed on 3 October 2017)
US_EPA_TSCA_12b	US EPA TSCA 12b			
Last_Update				
