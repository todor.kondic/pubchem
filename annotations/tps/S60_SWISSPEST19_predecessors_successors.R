# Mapping parent (predecessor) and metabolite (successor) pesticides
# Using S60_SWISSPEST.csv export from Kiefer et al 2019
# E. Schymanski 19 Feb 2020, updated 25-26 Feb 2020, 
# Updated again March 3, 2020 with final column names.
# full script with retrieving CIDs is SLE_Nov2019 dir. 

# set up dir etc. Make sure RChemMass is >v0.1.24. If not, grab it: 
# library(devtools)
# install_github("schymane/RChemMass", dependencies = F)
library(RChemMass)
tp_dir <- "C:/Users/emma.schymanski/Documents/GitHub/pubchem/tps/"
setwd(tp_dir)

### PART 1 Getting CIDs from PubChem: WARNING: SLOW ####
# NOTE THIS CHUNK OF CODE IS REALLY SLOW. DO NOT RERUN IF YOU DON'T HAVE TIME ... 
# find CID from SMILES (=> prefered tautomeric form)
swisspest <- read.csv(paste0(tp_dir,"S60_SWISSPEST19.csv"),stringsAsFactors = F)

swisspest$CID_SMILES <- ""
swisspest$ParentCID <- ""

i <- 1
for (i in 1:length(swisspest$Name)) {
  swisspest$CID_SMILES[i] <- getPCID.smiles(swisspest$SMILES_MS_Ready[i])
  swisspest$ParentCID[i] <- getPCIDs.CIDtype(swisspest$CID_SMILES[i],type="parent")
}

write.csv(swisspest,paste0(tp_dir,"S60_SWISSPEST19_wCIDs.csv"),row.names=F)

# Note: 26/2/2020: all CIDs are present for parents ... but not for all metabolites.
# March 3: fixed some CIDs (only in last column), most did not have structures, 
# or did not get CIDs, just SIDs
# saved fixed file as S60_SWISSPEST19_wCIDs_manFixes.csv



### Part 2: Parent / TP Mapping: START HERE IF TIME LIMITED ####
# read in fixed file with CIDs
swisspest <- read.csv(paste0(tp_dir,"S60_SWISSPEST19_wCIDs_manFixes.csv"),stringsAsFactors = F)

#subsetting according to parent/metabolite
sp_parents <- swisspest[which(swisspest$ParentOrMetabolite=="Parent"),]
sp_tps <- swisspest[which(swisspest$ParentOrMetabolite=="Metabolite"),]


# Map metabolites to parents
## the synonym column has parent_name metabolite. 
# Use "parent_name" part to find CID in parent subset
sp_tps$PredecessorName <- sub(" metabolite","",sp_tps$Synonym)
sp_tps$PredecessorName2 <- ""
sp_tps$PredecessorSMILES <- ""
sp_tps$PredecessorCID <- ""
sp_tps$PredecessorSMILESsource <- ""

i <- 1
for (i in 1:length(sp_tps$Name)) {
  sp_tps$PredecessorName2[i] <- strsplit(sp_tps$PredecessorName[i]," (",fixed=T)[[1]][1]
  #parent_i <- grep(sp_tps$ParentName[i],sp_parents$Name)
  parent_i <- grep(sp_tps$PredecessorName2[i],sp_parents$Name)

  # if parent name is not in parents list (e.g i=1), have to match by name
  # is there a function that returns the "best match" for a name?
  # below returns SMILES if it matches ... empty if not ... 
  if (length(parent_i)>0) {
    sp_tps$PredecessorSMILES[i] <- sp_parents$SMILES_MS_Ready[parent_i]
    sp_tps$PredecessorCID[i] <- sp_parents$ParentCID[parent_i]
    sp_tps$PredecessorSMILESsource[i] <- "sp_parents"
  } else {
    sp_tps$PredecessorSMILES[i] <- getPCproperty.IsoSMILES(sp_tps$PredecessorName2[i])$IsomericSMILES
    sp_tps$PredecessorCID[i] <- getPCproperty.IsoSMILES(sp_tps$PredecessorName2[i])$PCID
    sp_tps$PredecessorSMILESsource[i] <- "getPCproperty.IsoSMILES"
  }
}


### then, modify to file format PubChem needs
#sp_tps$Transformation <- "Metabolism"
sp_tps$Transformation <- "Environmental Transformation"
sp_tps$EvidenceDOI <- "10.1016/j.watres.2019.114972"
sp_tps$DatasetDOI <- "10.5281/zenodo.3544760"

sp_tp_colnames <- colnames(sp_tps)
sp_tp_colnames[13] <- "SuccessorCID"
sp_tp_colnames[14] <- "PredecessorSynonym"
sp_tp_colnames[15] <- "PredecessorName"
sp_tp_colnames[1] <- "SuccessorName"
sp_tp_colnames[9] <- "Source"
colnames(sp_tps) <- sp_tp_colnames
#Note: Evidence Description, Dataset Description and Source Description are
# provided separately in S60_SWISSPEST19_README.md

#Manual fixes from Karin:
#Abamectin: i=12
sp_tps$PredecessorCID[12] <- "6435890"
#BAS 700 F: 16095400
# In my SI this predecessor was called: BAS 700 F (Fluxapyroxad)
# i = 77, 78, 79
sp_tps$PredecessorName[c(77,78,79)] <- "Fluxapyroxad"
sp_tps$PredecessorCID[c(77,78,79)] <- "16095400"
#Carbentamide: The correct name is carbetamide (CID 27689)
sp_tps$PredecessorName[c(166,167,168,169)] <- "Carbetamide"
sp_tps$PredecessorCID[c(166,167,168,169)] <- "27689"
#Spinosad: Spinosad consists of spinosyn A (CID 443059) and spinosyn D (183094)
#Karin: I am not sure if the TPs originate from spinosyn A or D or both
#Emma: looks like one directly refers to Spinosyn D, so taken this CID for now. 
sp_tps$PredecessorCID[c(1038,1039)] <- "183094"


write.csv(sp_tps[,c("PredecessorName","PredecessorCID","Transformation","SuccessorName","SuccessorCID","EvidenceDOI","Source","DatasetDOI")],
          paste0(tp_dir,"S60_SWISSPEST_Predecessors_Successors_wCIDs.csv"),row.names = F)


#### PubChem functions: now added to RChemMass ####

### SMILES from Name
# https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/aspirin/property/isomericsmiles/JSON
# property table contains
# "CID": 2244
# "IsomericSMILES": "CC(=O)OC1=CC=CC=C1C(=O)O"
# but it may produce more than one CID
# e.g. carnitine CID 288, 85 and 5970
# Try to pub the "best" match first, but this is not always perfect. 
##modified from getPCIDs.CIDtype
#Note 5 March 2020: now added to RChemMass, this is now obselete. 

getPCproperty.IsoSMILES <- function(query, from = "name", to = "isomericsmiles", n=1, timeout=30)
{
  #build URL
  baseURL <- "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/"
  url <- paste0(baseURL, from, "/", query, "/property/", to, "/JSON")
  #https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/aspirin/property/isomericsmiles/JSON
  
  
  errorvar <- 0
  currEnvir <- environment()
  
  tryCatch(
    url_data <- getURL(URLencode(url),timeout=timeout),
    error=function(e){
      currEnvir$errorvar <- 1
    })
  
  if(errorvar){
    return(NA)
  }
  
  r <- fromJSON(url_data)
  
  if(!is.null(r$Fault)) {
    Properties <- list()
    Properties[['PCID']] <- NA
    Properties[['IsomericSMILES']] <- NA
    return(Properties)
  } else {
    PCID <- r$PropertyTable$Properties[[n]]$CID
    IsomericSMILES <- r$PropertyTable$Properties[[n]]$IsomericSMILES

    if (is.null(PCID)) {
      PCID <- NA
    }
    if (is.null(IsomericSMILES)) {
      IsomericSMILES <- NA
    }
    Properties <- list()
    Properties[['PCID']] <- PCID
    Properties[['IsomericSMILES']] <- IsomericSMILES
    return(Properties)
    
  }
}

#### getPCIDs.CIDtype - copied from another file ####
#Note March 5th, this is now in RChemMass. 

# parent / component etc
# https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/2735102/cids/JSON?cids_type=component
# cids_type
# 
# original, parent, component, similar_2d, similar_3d,
# same_stereo, same_isotopes,
# same_connectivity, same_tautomer,
# same_parent, same_parent _stereo, same_parent _isotopes,
# same_parent _connectivity, same_parent _tautomer
## if you have a mixture, "component" gives you the components of the mixture. 
## if you have an individual component, "component" gives you all the mixtures 
##  containing this component. 
## if I have a parent, I have the neutralized form. 
## if I don't have a parent, I want the components. 
##
## we assume that original returns the input
# > getPCIDs.CIDtype("3053",type="original")
# [1] 3053
## this is a salt, and returns a nice neutral parent
# > getPCIDs.CIDtype("167781",type="parent")
# [1] 167782
## if the parent is not available, go to "component" and get the bits
# > getPCIDs.CIDtype("104265",type="parent")
# [1] NA
# > getPCIDs.CIDtype("104265",type="component")
# [1] 13360  1004
## so, any CID should have a parent, or components. What about deprecated records?
# 4644
##
# > getPCIDs.CIDtype("4644",type="parent")
# [1] NA
# > getPCIDs.CIDtype("4644",type="original")
# [1] 4644
# > getPCIDs.CIDtype("4644",type="component")
# [1] NA

getPCIDs.CIDtype <- function(query, type="parent", from = "cid", to = "cids", timeout=30)
{
  # test type parameters
  if(!(type %in% c("original","parent","component", "preferred",
                   "similar_2d", "similar_3d", 
                   "same_stereo", "same_isotopes", "same_connectivity", "same_tautomer",
                   "same_parent", "same_parent_stereo", "same_parent_isotopes", 
                   "same_parent_connectivity", "same_parent_tautomer"))) {
    stop("Incorrect type: select one of original, parent, component, preferred, similar_2d, 
          similar_3d, same_stereo, same_isotopes, same_connectivity, same_tautomer,
          same_parent, same_parent_stereo, same_parent_isotopes, 
          same_parent_connectivity, same_parent_tautomer")
  }
  #build URL
  baseURL <- "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/"
  url <- paste0(baseURL, from, "/", query, "/", to, "/JSON?cids_type=", type)
  #https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/2735102/cids/JSON?cids_type=component
  
  
  errorvar <- 0
  currEnvir <- environment()
  
  tryCatch(
    url_data <- getURL(URLencode(url),timeout=timeout),
    error=function(e){
      currEnvir$errorvar <- 1
    })
  
  if(errorvar){
    return(NA)
  }
  
  # This happens if the PCID is not found:
  r <- fromJSON(url_data)
  
  if(!is.null(r$Fault)) {
    CIDs <- NA
    return(CIDs)
  } else {
    CIDs <- r$IdentifierList$CID
    
    if (is.null(CIDs)) {
      CIDs <- NA
    }
    return(CIDs)
  }
}




